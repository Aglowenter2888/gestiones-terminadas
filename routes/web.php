<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/capturaExp', function () {
    return view('expedientes.create');
});

Route::get('/header', function () {
    return view('layouts.header');
});

Route::get('/perfil', function () {
    return view('users.perfil');
});
Route::get('/tutoria', function () {
    return view('users.tutoria');
});

Route::get('/Gestioncademica', function () {
    return view('users.Gestioncademica');
});

Route::get('/editarperfil', function () {
    return view('users.editarperfil');
});
Route::get('/Productos_desarrolados', function () {
    return view('users.Productos_desarrolados');
});
Route::get('/capacitacion', function () {
    return view('users.capacitacion');
});
Route::get('/Eventos', function () {
    return view('users.Eventos');
});


Auth::routes();